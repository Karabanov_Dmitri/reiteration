package ru.dk.main;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;



public class Reiteration {


    public static void main(String[] args) {
        String pathToTheReadFile = "C:\\work\\Reiteration\\src\\ru\\dk\\file\\input.txt";
        String pathToTheRecordFile = "C:\\work\\Reiteration\\src\\ru\\dk\\file\\Output.txt";
        try (BufferedWriter output = new BufferedWriter(new FileWriter(pathToTheRecordFile))) {
            String[] readedMas = read(pathToTheReadFile);

            ArrayList<Integer> inputMas = new ArrayList<>();
            for (String i : readedMas) {
                inputMas.add(Integer.parseInt(i));
            }
            ArrayList<Integer> resultMas = sorter(inputMas);
            write(pathToTheRecordFile, resultMas);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static String[] read(String pathToTheReadFile) {
        try (BufferedReader reader = new BufferedReader(new FileReader(pathToTheReadFile))) {
            String readedString;
            String[] readedMas = new String[0];
            while ((readedString = reader.readLine()) != null) {
                readedMas = readedString.split(" ");
            }
            return readedMas;
        } catch (IOException e) {
            System.out.println(e);
        }
        return null;
    }


    private static void write(String pathToTheRecordFile, ArrayList<Integer> resultMas) {
        try (BufferedWriter output = new BufferedWriter(new FileWriter(pathToTheRecordFile))) {
            try {
                output.write(resultMas.toString() + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static ArrayList<Integer> sorter(ArrayList<Integer> mas) {
        ArrayList<Integer> masOfNums = new ArrayList<>();
        ArrayList<Integer> numberOfRepetitions = new ArrayList<>();
        ArrayList<Integer> resultMas = new ArrayList<>();
        int i = 0;
        while (mas.size() != 0) {
            masOfNums.add(mas.get(0));
            numberOfRepetitions.add(Collections.frequency(mas, mas.get(0)));
            while (mas.indexOf(masOfNums.get(i)) != -1)
                mas.remove(masOfNums.get(i));
            i++;
        }
        for (i = 0; i < masOfNums.size(); i++) {
            for (int j = 1; j < masOfNums.size() - i; j++) {
                if (numberOfRepetitions.get(j - 1) < numberOfRepetitions.get(j)) {
                    int tmp = numberOfRepetitions.get(j - 1);
                    numberOfRepetitions.set(j - 1, numberOfRepetitions.get(j));
                    numberOfRepetitions.set(j, tmp);
                    tmp = masOfNums.get(j - 1);
                    masOfNums.set(j - 1, masOfNums.get(j));
                    masOfNums.set(j, tmp);
                }
            }
        }
        for (i = 0; i < masOfNums.size(); i++) {
            for (int j = 0; j < numberOfRepetitions.get(i); j++) {
                resultMas.add(masOfNums.get(i));
            }
        }
        return resultMas;
    }

}
