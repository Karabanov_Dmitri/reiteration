package ru.dk.main;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ReiterationTest {

    @Test
    public void main() {

        {
            int[] mass = {2, 4, 5, 0, 0, 1, 2, 2, 5, 0, 0};
            int[]mass1={0, 0, 0, 0, 2, 2, 2, 5, 5, 4, 1};
            ArrayList<Integer> outputMas = new ArrayList<>();
            for (int i : mass1) {
                outputMas.add(i);
            }
            ArrayList<Integer> inputMas = new ArrayList<>();
            for (int i : mass) {
                inputMas.add(i);
            }

            Reiteration reiteration = new Reiteration();
            assertEquals(outputMas, reiteration.sorter(inputMas));

        }
    }
}